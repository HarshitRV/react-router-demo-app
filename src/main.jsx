import * as React from "react";
import * as ReactDOM from "react-dom/client";
import {
	createBrowserRouter,
	createHashRouter,
	RouterProvider,
	createRoutesFromElements,
	Route,
} from "react-router-dom";
import "./index.css";
import Root from "./routes/root";
import ErrorPage from "./error-page";
import Contact from "./routes/contact";
import EditContact from "./routes/edit";
import Index from "./routes";
import { loader as rootLoader, action as rootAction } from "./routes/root";
import { loader as contactLoader } from "./routes/contact";
import { action as editAction } from "./routes/edit";
import { action as destroyContact } from "./routes/destroyContact";
import { action as contactAction } from "./routes/contact";

// Way #1 of creating routes
const router = createBrowserRouter([
	{
		path: "/",
		element: <Root />,
		errorElement: <ErrorPage />,
		loader: rootLoader,
		action: rootAction,
		children: [
			{
				errorElement: <ErrorPage />,
				children: [
					{
						index: true,
						element: <Index />,
					},
					{
						path: "contacts/:contactId",
						element: <Contact />,
						loader: contactLoader,
						action: contactAction,
					},
					{
						path: "contacts/:contactId/edit",
						element: <EditContact />,
						loader: contactLoader,
						action: editAction,
					},
					{
						path: "contacts/:contactId/destroy",
						action: destroyContact,
						errorElement: <div>Oops! There was an error.</div>,
					},
				],
			},
		],
	},
]);

// Way #2 of creating routes, could use the createBrowserRouter or createHashRouter
const router2 = createHashRouter(
	createRoutesFromElements(
		<Route
			path="/"
			element={<Root />}
			errorElement={<ErrorPage />}
			loader={rootLoader}
			action={rootAction}>
			<Route errorElement={<ErrorPage />}>
				<Route
					index
					element={<Index />}
				/>
				<Route
					path="contacts/:contactId"
					element={<Contact />}
					loader={contactLoader}
					action={contactAction}
				/>
				<Route
					path="contacts/:contactId/edit"
					element={<EditContact />}
					loader={contactLoader}
					action={editAction}
				/>
				<Route
					path="contacts/:contactId/destroy"
					action={destroyContact}
					errorElement={<div>Oops! There was an error.</div>}
				/>
			</Route>
		</Route>
	)
);

ReactDOM.createRoot(document.getElementById("root")).render(
	<React.StrictMode>
		<RouterProvider router={router2}></RouterProvider>
	</React.StrictMode>
);
